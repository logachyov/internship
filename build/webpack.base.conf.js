const path = require('path');
const fs = require('fs');
const webpack = require('webpack')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const PATHS = {
    src: path.resolve(__dirname, '../src'),
    dist: path.resolve(__dirname, '../public'),
    assets: 'assets/'
}

const PAGES_DIR = PATHS.src;
const PAGES = fs.readdirSync(PAGES_DIR).filter(fileName => fileName.endsWith('.html'));

module.exports ={
    context: path.resolve(__dirname),
    externals: {
        paths: PATHS
    },
    entry: {
        app: '../src/index.js',
    },
    output: {
        filename: `js/[name].[contenthash].js`,
        path: PATHS.dist,
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                  name: 'vendors',
                  test: /node_modules/,
                  chunks: 'all',
                  enforce: true
                }
            }
        }
    },
    module: {
        rules: [
        // JS
        {
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: '/node_modules'
        },
        // Fonts
        {
            test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: '/assets/fonts/'
            }
        },
        // Img
        {
            test: /\.(png|jpg|gif|svg)$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: '/assets/img/'
            }
        },
        // Video
        // {
        //     test: /\.(mov|mp4)$/,
        //     use: [
        //       {
        //         loader: 'file-loader',
        //         options: {
        //           name: '[name].[ext]',
        //           outputPath: '/assets/video/'
        //         }
        //       },
        //     ]
        // },
        {
            test: /\.html$/,
            use: {
                loader: "html-loader",
                options: {
                  attributes: {
                      list: [
                          {
                            attribute: 'src',
                            type: 'src',
                            filter: (tag, attribute, attributes, resourcePath) => {
                                return tag.toLowerCase() !== 'img';
                              },
                          }
                      ]
                  }
                }
            }
        },
        // SCSS
        {
            test: /\.scss$/,
            use: [
                "style-loader",
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: {
                        sourceMap: true,
                        url: false
                    }
                }, {
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: true,
                        config: {
                            path: './postcss.config.js'
                        }
                    }
                }, {
                    loader: 'resolve-url-loader',
                    options: {
                        sourceMap: true
                    }
                }, {
                    loader: 'sass-loader',
                    options: {
                        sourceMap: true
                    }
                }
            ],
        },
        // CSS
        {
            test: /\.css$/,
            use: [
                'style-loader',
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: {
                        sourceMap: true,
                        url: false
                    }
                }, {
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: true,
                        config: {
                            path: './postcss.config.js'
                        }
                    }
                },
            ],
        }]
    },
    resolve: {
        alias: {
        //   '~': PATHS.src
        }
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: `css/[name].[contenthash].css`
        }),
        new CopyWebpackPlugin ({

            patterns: [
                // Static
                {
                    from: `${PATHS.src}/static`,
                    to: ''
                },
            ]
        }),
        new CleanWebpackPlugin(),
        ...PAGES.map(
            page =>
              new HtmlWebpackPlugin({
                template: `${PAGES_DIR}/${page}`,
                filename: `./${page}`,
                inject: true
            })
        )
    ],
}