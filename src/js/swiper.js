import Swiper, { Navigation, Pagination } from 'swiper';
import 'swiper/swiper-bundle.css';

Swiper.use([Navigation, Pagination]);

const mainSlider = document.querySelectorAll('.slider-control.swiper-container');
const secondnarySlider = document.querySelectorAll('.slider-control__slidein.swiper-container');

// Main slider
mainSlider.forEach((element) => {
  const paginationElements = element.querySelectorAll('.slider-control__item[data-slide]');
  const paginationElementsAll = Array.from(paginationElements);

  let menu = paginationElementsAll.map((el) => {
    return el.dataset.slide;
  })

  let mySwiperProjects = new Swiper(element, {
    // Optional parameters main slider
    loop: true,
    simulateTouch: false,
    autoHeight: true,
    // Pagination main slider
    pagination: {
      el: '.slider-control__pagination-up, .slider-control__pagination-down',
      bulletClass: 'slider-control__bullet',
      bulletActiveClass: 'slider-control__bullet-active',
      modifierClass: 'slider-control-',
      clickable: true,
      renderBullet: function (index, className) {
        return '<span class="' + className + '">' + '<span class="slider-control__bullet-up">' + (menu[index]) + '</span>' + '</span>'
      },
    },

    // Navigation arrows main slider
    navigation: {
      nextEl: '.slider-control__button--next',
      prevEl: '.slider-control__button--prev',
    },
  })
})
// Secondnary slider
secondnarySlider.forEach((element) => {
  let mySwiperProjectsMini = new Swiper(element, {
    // Optional parameters secondnary slider
    slidesPerView: 'auto',
    spaceBetween: 21,
    // Navigation arrows secondnary slider
    navigation: {
      nextEl: '.slider-control__slidein-btn--next',
      prevEl: '.slider-control__slidein-btn--prev',
      disabledClass: 'slider-control__slidein-disable'
    },
  })
})
// Tabs slider
const mediaQuery = window.matchMedia('(max-width: 900px)');

const handleTabletChange = (e) => {
  const tabSlider = document.querySelectorAll('.tabs__row');
  let tabsSwiper;

  tabSlider.forEach((element) => {

    if (e.matches === true) {
      element.tabsSwiper = new Swiper (element, {
        loop: true,
        observer: true,
        observeParents: true,
        slideClass: 'tabs__slider',
        wrapperClass: 'tabs__col',
        // Navigation arrows tabs slider
        navigation: {
          nextEl: '.tabs__slidein-btn--next',
          prevEl: '.tabs__slidein-btn--prev',
        },
      })
    } else if(e.matches === false) {
      if (element.tabsSwiper !== undefined) {
        element.tabsSwiper.destroy(true, true);
      }
      return;
    }
  })
}

mediaQuery.addListener(handleTabletChange);

handleTabletChange(mediaQuery);
// Steps slider
const stepSlider = document.querySelectorAll('.steps-slider.swiper-container');

stepSlider.forEach((element) => {

  const slideCurrent = element.querySelector('.steps-slider__counter');

  const paginationElements = element.querySelectorAll('.steps-slider__slide[data-step]');
  const paginationElementAll = Array.from(paginationElements);

  let steps = paginationElementAll.map((el) => {
    return el.dataset.step;
  })

  let swiperSteps = new Swiper(element, {
    // Optional parameters main slider
    simulateTouch: false,
    autoHeight: true,
    // slideClass: 'steps-slider__slide',
    // wrapperClass: 'steps-slider__wrapper',
    // Pagination main slider
    pagination: {
      el: '.steps-slider__pagination-up, .steps-slider__pagination-down',
      bulletClass: 'steps-slider__bullet',
      bulletActiveClass: 'steps-slider__bullet-active',
      modifierClass: 'steps-slider-',
      clickable: true,
      renderBullet: function (index, className) {
        return '<div class="' + className + '">\
        <span class="steps-slider__num">' + (index < 10 ? '0' + (index + 1) : (index + 1)) + '</span>\
        <span class="steps-slider__title">' + (steps[index]) + '</span>\
        </div>';
      }
    },

    // Navigation arrows main slider
    navigation: {
      nextEl: '.steps-slider__button--next',
      prevEl: '.steps-slider__button--prev',
    },
  })

  swiperSteps.on('slideChange', () => {
    changeStep();
  });

  const changeStep = () => {
    const current = swiperSteps.realIndex + 1;
    slideCurrent.innerHTML = current + '/' + swiperSteps.slides.length;
  }

  changeStep();
})