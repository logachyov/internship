const menu = document.querySelectorAll('.menu--header');

menu.forEach((element) => {

    const menuBtn = element.querySelector('.menu__mobile-btn');
    const menuList = element.querySelector('.menu__content');
    const lockBody = document.body

    menuBtn.addEventListener('click', () => {
        menuBtn.classList.toggle('open');
        menuList.classList.toggle('open');
        lockBody.classList.toggle('lock');
        element.classList.remove('animateIn')
    })
})

// Menu sticky
let startOffset = 0;
const bannerHeight = 83;
const bannerWrapper = document.querySelector('.menu--header');

const handler = () => {
    const newOffset = window.scrollY || window.pageYOffset;

    if (newOffset > bannerHeight) {
        if (newOffset > startOffset) {
            bannerWrapper.classList.remove('animateIn');
            bannerWrapper.classList.add('animateOut');
        } else {
            bannerWrapper.classList.remove('animateOut');
            bannerWrapper.classList.add('animateIn');
        }
        bannerWrapper.classList.add('bg-color');
        startOffset = newOffset;
    } else {
        bannerWrapper.classList.remove('bg-color');
    }
};

window.addEventListener('scroll', handler, false);