// Scroll down btn
const scrollBtn = document.querySelector('.scroll');

const scrollDown = () => {

    const windowCoords = document.documentElement.clientHeight;

    const scroll = () => {
        if (window.pageYOffset < windowCoords) {
            window.scrollBy(0, 10);
            setTimeout(scroll, 3);
        }
    }

    scroll();
}

scrollBtn.addEventListener('click', scrollDown);
// Anchor scroll
const anchors = document.querySelectorAll('.menu__list-link[href*="#"]');

anchors.forEach((item) => {
    item.addEventListener('click', (e) => {
        e.preventDefault();

        let anchorsAttr = document.querySelector(item.getAttribute('href'));

        anchorsAttr.scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        })
    });
})

window.addEventListener('scroll', () => {

    const blocksContent = document.querySelectorAll(".nav-id[id]");

    blocksContent.forEach((block) => {
        const id = block.getAttribute('id');

        const link = document.querySelector(`.menu__list-link[href*="#${id}"]`);

        var positionTop = block.getBoundingClientRect().top-83;
        var positionBottom = block.getBoundingClientRect().bottom-83;

        if (positionTop < 0) {
            link.classList.add("active");
        } else {
            link.classList.remove("active");
        };
        if (positionBottom < 0) {
            link.classList.remove("active");
        }
    })
})