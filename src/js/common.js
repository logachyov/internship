import 'core-js';
import 'classlist-polyfill';
import 'fullscreen-api-polyfill';
import './menuMobile';
import './scroll';
import './videoPlayer';
import './swiper';
import './tabs';
import './popup';

import objectFitImages from 'object-fit-images';
import smoothscroll from 'smoothscroll-polyfill';

objectFitImages();

smoothscroll.polyfill();