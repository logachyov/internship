const tabs = document.querySelectorAll('.tabs');

tabs.forEach((element) => {
    const triggers = element.querySelectorAll('.tabs__trigger');

    triggers.forEach((trigger) => {
        trigger.addEventListener('click', function() {
        const id = this.getAttribute('data-item');

        const activeTrigger = element.querySelector('.tabs__trigger.tab-active');

        const content = element.querySelector('.tabs__row[data-item="'+id+'"]');
        const activeContent = element.querySelector('.tabs__row.tab-active');

        const contentImg = element.querySelector('.tabs__img-content[data-item="'+id+'"]');
        const activeImg = element.querySelector('.tabs__img-content.tab-active');

        activeTrigger.classList.remove('tab-active');
        trigger.classList.add('tab-active');

        activeContent.classList.remove('tab-active');
        content.classList.add('tab-active');

        activeImg.classList.remove('tab-active');
        contentImg.classList.add('tab-active');
        });
    });
})