const playerForVideo = document.querySelectorAll('.video-player');

playerForVideo.forEach((elem) => {
  const videoContainer = elem.querySelector('.video-player__container');
  const video = elem.querySelector('.video-player__video');
  // Play button
  const playPauseAction = elem.querySelector('.video-player__start');
  const playAction = elem.querySelector('.video-player__start-btn');
  // Button controls player
  const controlsContainer = elem.querySelector('.video-player__controls');
  const playPauseButton = elem.querySelector('.video-player__play-pause');
  const volumeButton = elem.querySelector('.video-player__volume');
  const fullScreenButton = elem.querySelector('.video-player__full-screen');
  const playButton = playPauseButton.querySelector('.video-player__playing');
  const pauseButton = playPauseButton.querySelector('.video-player__paused');
  const fullVolumeButton = volumeButton.querySelector('.video-player__full-volume');
  const mutedButton = volumeButton.querySelector('.video-player__muted');
  const maximizeButton = fullScreenButton.querySelector('.video-player__maximize');
  const minimizeButton = fullScreenButton.querySelector('.video-player__minimize');
  // Progress player
  const progressBar = elem.querySelector('.video-player__progress-bar');
  const watchedBar = elem.querySelector('.video-player__watched-bar');
  const timeLeft = elem.querySelector('.video-player__time-remaining');

  let controlsTimeout;

  // Controls show/hidden
  const displayControls = () => {
    controlsContainer.classList.add('show')
    elem.classList.remove('hidden');
    if (controlsTimeout) {
      clearTimeout(controlsTimeout);
    }
    controlsTimeout = setTimeout(() => {
      controlsContainer.classList.remove('show')
      elem.classList.add('hidden');
    }, 3000);
  };

  // Play-pause
  const playPause = () => {
    if (video.paused) {
      video.play();
      playButton.classList.remove('active');
      pauseButton.classList.add('active');
      playAction.classList.remove('active');
      playPauseAction.classList.remove('play');
      controlsContainer.classList.add('active');
    } else {
      video.pause();
      playButton.classList.add('active');
      pauseButton.classList.remove('active');
      playAction.classList.add('active');
      controlsContainer.classList.remove('active');
      playPauseAction.classList.add('play');
    }
  };

  // Mute
  const toggleMute = () => {
    video.muted = !video.muted;
    if (video.muted) {
      fullVolumeButton.classList.remove('active');
      mutedButton.classList.add('active');
    } else {
      fullVolumeButton.classList.add('active');
      mutedButton.classList.remove('active');
    }
  };

  // Fullscreen
  const toggleFullScreen = () => {
    if (!document.fullscreenElement) {
      videoContainer.requestFullscreen();
    } else {
      document.exitFullscreen();
    }
  };

  elem.addEventListener('fullscreenchange', () => {
    if (!document.fullscreenElement) {
      maximizeButton.classList.add('active');
      minimizeButton.classList.remove('active');
      video.classList.remove('heigth-none');
    } else {
      maximizeButton.classList.remove('active');
      minimizeButton.classList.add('active');
      video.classList.add('heigth-none');
    }
  });

  // Mouse movement
  elem.addEventListener('mousemove', () => {
    displayControls();
  });

  // Progress video
  video.addEventListener('timeupdate', () => {
    let minutes = Math.floor(video.currentTime / 60),
    seconds = Math.floor(video.currentTime - minutes * 60),
    x  = minutes < 10 ? "0" + minutes : minutes,
    y  = seconds < 10 ? "0" + seconds : seconds;

    watchedBar.style.width = ((video.currentTime / video.duration) * 100) + '%';
    timeLeft.textContent = x + " : " + y;
    });

  // Click
  progressBar.addEventListener('click', (event) => {
    const pos = (event.offsetX / progressBar.offsetWidth) * video.duration;
    video.currentTime = pos;
  });

  playPauseButton.addEventListener('click', playPause);

  playPauseAction.addEventListener('click', playPause);

  playAction.addEventListener('click', playPause);

  volumeButton.addEventListener('click', toggleMute);

  fullScreenButton.addEventListener('click', toggleFullScreen);
})