const popupRequest = document.querySelector('.request-popup');
const popupOverlayClose = document.querySelector('.request-popup__overlay');
const popupRequestClose = document.querySelector('.form-feedback__popup-close');
const popupRequestOpen = document.querySelectorAll('.btn-all--menu');
const lockBodyPopup = document.body;

const openPopup = () => {
    popupRequest.classList.add('show');
    lockBodyPopup.classList.add('lock-popup');
}

const closePopup = () => {
    popupRequest.classList.remove("show");
    lockBodyPopup.classList.remove('lock-popup');
}

popupRequestOpen.forEach((element) => {
    element.addEventListener('click', openPopup);
});

popupRequestClose.addEventListener('click', closePopup);
popupOverlayClose.addEventListener('click', closePopup);
