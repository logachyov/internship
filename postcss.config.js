module.exports = {
    plugins: [
      require('autoprefixer'),
      require('postcss-object-fit-images'),
      require('cssnano')({
          preset: [
              'default', {
                  discardComments: {
                      removeAll: true,
                  }
              }
          ]
      }),
    ]
}